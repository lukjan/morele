<?php

/*	if (isset($_POST['form_submit']))
	{
		echo "<pre>";
		print_r($_FILES);
		print_r($_POST);
		echo "</pre>";
		echo '<hr>';

		$katalogUpload = __DIR__ . '\upload\\';
		$plikUpload = $katalogUpload . $_FILES['upload_image']['name'];

		if ( ($_FILES['upload_image']['type'] != 'image/jpeg') && ($_FILES['upload_image']['size'] != 0) )
		{
			$result ='<div class="alert alert-error"><strong>Proszę naprawić następujące błędy: </strong>- Wybrano niewłaściwy format pliku</div>';
		}
		else
		{
			if ($_FILES['upload_image']['error'] > 0)
			{
			    switch ($_FILES['upload_image']['error'])
			    {
			      case 1: { $error="<br>- Rozmiar pliku jest dużo za duży."; break; }
			      case 2: { $error="<br>- Rozmiar pliku jest zbyt duży."; break; }
			      case 3: { $error="<br>- Plik wysłany tylko częściowo."; break; }
			      case 4: { $error="<br>- Nie wysłano żadnego pliku."; break; }
			      default: { $error="<br>- Wystąpił błąd podczas wysyłania."; break; }
			    }
			}
			if (isset($error))
			{
				$result ='<div class="alert alert-error"><strong>Proszę naprawić następujące błędy: </strong> '.$error.'</div>';
			}
			else
			{
				(move_uploaded_file($_FILES['upload_image']['tmp_name'], $plikUpload));
				$result ='<div class="alert alert-success"><strong>Plik został przesłany pomyślnie.</strong></div>';
			}
		}
	}


	require __DIR__.'\vendor\\autoload.php';

	use Monolog\Logger;
	use Monolog\Handler\StreamHandler;
	use Monolog\Handler\FirePHPHandler;

	// Create the logger
	$logger = new Logger('send_form_info');
	// Now add some handlers
	$logger->pushHandler(new StreamHandler(__DIR__.'\log.txt', Logger::INFO));
	$logger->pushHandler(new FirePHPHandler());

	// You can now use your logger
	$logger->info('My logger is now ready');
*/
?>
<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<title>Zadanie rekrutacyjne</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Formularz do wysyłki">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400&amp;subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>

	<section id="aw-contact" class="aw-section">
		<div class="container">
			<h1>Formularz</h1>

			<?php echo @$result; ?>

			<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">

				<fieldset>
					<legend>Formularz wysyłania zdjęcia</legend>
					<input type="hidden" name="MAX_FILE_SIZE" value="1048576" />

					<label for="sendFile">Wybierz plik do wysłania</label>
					<input type="file" name="upload_image" id="sendFile" required><br>

					<label for="width">Width</label>
					<input type="number"  min="0" max="4096" name="new_width" id="width" placeholder="Podaj szerokość" required><br>

					<label for="height">Height</label>
					<input type="number"  min="0" max="3112" name="new_height" id="height" placeholder="Podaj wysokość" required><br>

					<input type="submit" name="form_submit" value="Prześlij plik">
				</fieldset>

			</form>

			<?php
				require __DIR__.'\vendor\\autoload.php';

				use Monolog\Logger;
				use Monolog\Handler\StreamHandler;
				use Monolog\Handler\FirePHPHandler;

				// Create the logger
				$logger = new Logger('send_form_info');
				// Now add some handlers
				$logger->pushHandler(new StreamHandler(__DIR__.'\log.txt', Logger::INFO));
				$logger->pushHandler(new FirePHPHandler());

				// You can now use your logger
				$logger->info('My logger is now ready');

				function resizeImage($resourceType,$image_width,$image_height,$resizeWidth,$resizeHeight) {
				    // $resizeWidth = 100;
				    // $resizeHeight = 100;
				    $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
				    imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
				    return $imageLayer;
				}

				if(isset($_POST["form_submit"])) {
					$imageProcess = 0;
				    if(is_array($_FILES)) {
				        $new_width = $_POST['new_width'];
				        $new_height = $_POST['new_height'];
				        $fileName = $_FILES['upload_image']['tmp_name'];
				        $sourceProperties = getimagesize($fileName);
				        $resizeFileName = time();
				        $uploadPath = "./upload/";
				        $fileExt = pathinfo($_FILES['upload_image']['name'], PATHINFO_EXTENSION);
				        $uploadImageType = $sourceProperties[2];
				        $sourceImageWidth = $sourceProperties[0];
				        $sourceImageHeight = $sourceProperties[1];
				        switch ($uploadImageType) {
				            case IMAGETYPE_JPEG:
				                $resourceType = imagecreatefromjpeg($fileName); 
				                $imageLayer = resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
				                imagejpeg($imageLayer,$uploadPath."thump_".$resizeFileName.'.'. $fileExt);
				                break;

				            case IMAGETYPE_GIF:
				                $resourceType = imagecreatefromgif($fileName); 
				                $imageLayer = resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
				                imagegif($imageLayer,$uploadPath."thump_".$resizeFileName.'.'. $fileExt);
				                break;

				            case IMAGETYPE_PNG:
				                $resourceType = imagecreatefrompng($fileName); 
				                $imageLayer = resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
				                imagepng($imageLayer,$uploadPath."thump_".$resizeFileName.'.'. $fileExt);
				                break;

				            default:
				                $imageProcess = 0;
				                break;
				        }
				        move_uploaded_file($fileName, $fileExt);
				        $imageProcess = 1;
				    }

					if($imageProcess == 1){
					?>
					<div class="row">
							
							<div>
								<h4><b>Przesłane przesalowane zdjęcie</b></h4>

								<p>
									<a href="<?php echo $uploadPath."thump_".$resizeFileName.'.'. $fileExt; ?>" download class="btn btn-danger"><i class="fa fa-download"></i>Pobierz</a href="">
								</p>
								<img class="img-rounded img-responsive" src="<?php echo $uploadPath."thump_".$resizeFileName.'.'. $fileExt; ?>" width="<?php echo $new_width; ?>" height="<?php echo $new_height; ?>" >

							</div>
						</div>

					<?php
					}else{
					?>
						<div class="alert icon-alert with-arrow alert-danger form-alter" role="alert">
							<i class="fa fa-fw fa-times-circle"></i>
							<strong> Note !</strong> <span class="warning-message">Invalid Image </span>
						</div>
					<?php
					}
					$imageProcess = 0;
				}
			?>
			
			<div class="info">
				<h3>Informacje:</h3>
				<p>Dopuszczalny format pliku: JPG, PNG, GIF. <br>Maksymalny rozmiar pliku wynosi: 1 MB</p>
			</div>
			
			<div>
				<h2>Wszystkie przesłane grafiki</h2>
				<?php
					$file = glob('upload/*.*');
					foreach ($file as $key => $value) {
						echo '<img src="'.$value.'" alt="">';
					}
				?>
			</div>

		</div>
	</section>

</body>
</html>